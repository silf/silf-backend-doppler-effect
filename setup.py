from distutils.core import setup
from configparser import ConfigParser
from os import path

from pip.req import parse_requirements

# parse_requirements() returns generator of pip.req.InstallRequirement objects
install_reqs = parse_requirements(path.join(path.dirname(__file__), 'requirements.txt'))

if __name__ == "__main__":

    cp = ConfigParser()
    file = path.join(path.dirname(__file__), "silf.backend.doppler/version.ini")
    with open(file) as f:
        cp.read_file(f)

    setup(
        name='silf-backend-doppler-effect',
        version=cp['VERSION']['VERSION'],
        packages=['silf.backend.doppler'],
        url='',
        license='',
        author='Silf Team',
        author_email='',
        description='',
        install_requirements=[str(ir.req) for ir in install_reqs]
    )
