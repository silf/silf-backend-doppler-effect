import serial
from silf.backend.doppler.api import DopplerEffectAPI


OK_RESULT = 'OK'


class DopplerEffectDriver(DopplerEffectAPI):
    def __init__(self, deviceFilePath, readTimeout=10):
        self.devSerial = serial.Serial(deviceFilePath, baudrate=115200, timeout=readTimeout,
                                       parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, xonxoff=False)
        if not self.devSerial.isOpen():
            raise ValueError("Can not open device: " + deviceFilePath)
        self.velocity = 0
        self.freq = 0
        self.__triggering = False
        self.logger = None

    def start(self):
        self.devSerial.flush()

    def _run_cmd(self, cmd):
        if cmd[-1] != '\n':
            cmd += '\n'

        byte_cmd = bytes(cmd, "UTF-8")
        self.logger.debug("Run_CMD: "+str(byte_cmd))
        byte_written = self.devSerial.write(byte_cmd)
        self.logger.debug("Len: "+str(byte_written))
        if len(byte_cmd) != byte_written:
            raise ValueError("Error while communicating with device, bytes written " + str(byte_written)
                             + " failed command is: " + cmd)
        ret = self.devSerial.readline().decode("utf-8")
        self.logger.debug("RunCMD ret: "+str(ret))
        return ret

    def get_device_model(self):
        return 'DopplerEffect\n'

    def set_velocity(self, velocity):
        self.velocity = velocity
        ret = self._run_cmd("p {}".format(velocity))
        if ret[:2] != OK_RESULT[:2]:
            raise ValueError("Error while communicating with device, set_velocity not confirmed")

    def set_approach(self):
        byte_written = self.devSerial.write(bytes("a\n", "UTF-8"))
        if byte_written != 2:
            raise ValueError("Error while communicating with device, set_approach sending")
        ret = self.devSerial.readline().decode("utf-8")
        self.logger.debug("Approach ret: "+str(ret))
        if ret[:2] != OK_RESULT[:2]:
            raise ValueError("Error while communicating with device, set_approach not confirmed")
        self.__triggering = False

    def trigger(self):
        self.__triggering = True
        self.set_approach()
        byte_written = self.devSerial.write(bytes("t\n", "UTF-8"))
        if byte_written != 2:
            raise ValueError("Error while communicating with device, trigger sending")
        ret = self.devSerial.readline().decode("utf-8")
        self.logger.debug("Trig ret: "+str(ret))
        if ret[:2] != OK_RESULT[:2]:
            print("ret: {}".format(ret))
            raise ValueError("Error while communicating with device, trigger not confirmed")
        self.__triggering = False

    @property
    def is_triggering(self):
        return self.__triggering

    def get_velocity(self):
        self.velocity = float(self._run_cmd("v"))
        ret = self.devSerial.readline().decode("utf-8")
        if ret[:2] != OK_RESULT[:2]:
            raise ValueError("Error while communicating with device, stop cmd not confirmed")
        # scale velocity; temporary it should be in arduino
        self.velocity = self.velocity+9+self.velocity/4
        return self.velocity

    def get_time(self):
        t = float(self._run_cmd("w"))
        ret = self.devSerial.readline().decode("utf-8")
        if ret[:2] != OK_RESULT[:2]:
            raise ValueError("Error while communicating with device, stop cmd not confirmed")
        return t

    def get_freq(self):
        self.freq = float(self._run_cmd("f"))
        ret = self.devSerial.readline().decode("utf-8")
        if ret[:2] != OK_RESULT[:2]:
            raise ValueError("Error while communicating with device, stop cmd not confirmed")
        return self.freq

    def measure_frequency_zero(self):
        self.freq = float(self._run_cmd("F"))
        ret = self.devSerial.readline().decode("utf-8")
        if ret[:2] != OK_RESULT[:2]:
            raise ValueError("Error while communicating with device, stop cmd not confirmed")
        return self.freq

    def stop(self):
        ret = self._run_cmd("s")
        self.logger.debug("Stop ret: "+str(ret))
        if ret[:2] != OK_RESULT[:2]:
            raise ValueError("Error while communicating with device, stop cmd not confirmed")

    def is_open(self):
        return self.devSerial.isOpen()

    def close(self):
        self.devSerial.close()
