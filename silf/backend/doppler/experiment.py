# coding=utf-8

import os
import logging
import typing
import configparser
from collections import namedtuple

from enum import Enum

from abc import abstractmethod
from silf.backend.commons.new.result import ChartXYCreator
from silf.backend.commons.device_manager._result_creator import XYChartResultCreator, OverrideResultsCreator

from silf.backend.commons.api.stanza_content import ControlSuite, NumberControl, ComboBoxControl, \
    OutputFieldSuite, OutputField, ChartField

from silf.backend.commons.api.exceptions import ValidationError
from silf.backend.commons.api.stanza_content._error import Error
from silf.backend.commons.simple_server.simple_server import ExperimentMode, MultiModeManager, ResultManager

from silf.backend.doppler.driver import DopplerEffectDriver
from silf.backend.doppler.mock_driver import DopplerEffectMockDriver
from silf.backend.commons.api.translations.trans import _


class BaseMode(ExperimentMode):

    CONFIG_SECTION = 'doppler_effect_device'
    device_id = 'doppler_effect_device'

    result = {'voltage': 0, 'current': 0}

    class FSM(object):
        def __init__(self, driver, config):
            self.driver = driver
            self.device_config = config

        class States(Enum):
            IDLE = "idle"
            SET_V = "set_v"
            TRIGGER = "trigger"
            WAIT = "wait"
            READ_RESULT = "read_result"

        class RetCode(Enum):
            REPEAT = 1
            BRANCH1 = 2
            BRANCH2 = 3

        Transition = namedtuple('Transition', ['src', 'ret_code', 'dest'])

        transitions = [
            Transition(States.IDLE, RetCode.REPEAT, States.IDLE),
            Transition(States.IDLE, RetCode.BRANCH1, States.SET_V),
            Transition(States.SET_V, RetCode.BRANCH1, States.TRIGGER),
            Transition(States.TRIGGER, RetCode.BRANCH1, States.WAIT),
            Transition(States.WAIT, RetCode.REPEAT, States.WAIT),
            Transition(States.WAIT, RetCode.BRANCH1, States.READ_RESULT),
            Transition(States.READ_RESULT, RetCode.BRANCH1, States.SET_V),
            Transition(States.READ_RESULT, RetCode.BRANCH2, States.IDLE),
        ]

        state = States.IDLE

        def _transition_lookup(self, src_state, ret_code):
            for t in self.transitions:
                if t.src is src_state and t.ret_code is ret_code:
                    return t.dest
            return None
        
        def _execute_state_function(self, state):
            return getattr(self, '_' + state.name.lower())()

        def process(self):
            ret_code = self._execute_state_function(self.state)
            new_state = self._transition_lookup(self.state, ret_code)
            if self.state is not new_state:
                print("Transition: {} -> {}".format(self.state, new_state))
            self.state = new_state

        start = False
        finished = False
        result_valid = False
        settings = {'start_v': 0, 'end_v': 0}
        result = {'v': 0, 'f': 0}
        point = 0
        max_points = 0

        def _idle(self):
            self.point = 0
            if self.start:
                self.finished = False
                self.result_valid = False
                self.start = False
                return self.RetCode.BRANCH1
            else:
                return self.RetCode.REPEAT

        def _set_v(self):
            v = self.settings['start_v'] + self.point * (self.settings['end_v'] - self.settings['start_v']) / self.max_points
            print("v = {}".format(v))
            self.driver.set_velocity(velocity=v)
            return self.RetCode.BRANCH1

        def _trigger(self):
            self.result_valid = False
            self.driver.trigger()
            return self.RetCode.BRANCH1

        def _wait(self):
            if self.driver.is_triggering:
                return self.RetCode.REPEAT
            else:
                return self.RetCode.BRANCH1

        def _read_result(self):
            # time in ms
            t = self.driver.get_time()
            v = float(self.device_config['sensor_distance'])/t*1000.0
            self.result = {'v': v, 'f': self.driver.get_freq()}
            self.point += 1
            self.result_valid = True
            if self.point < self.max_points:
                return self.RetCode.BRANCH1
            else:
                self.finished = True
                return self.RetCode.BRANCH2

    def __init__(self, config_parser, experiment_callback):
        super().__init__(config_parser, experiment_callback)
        self.started = False
        self.doppler_driver = None
        self.fsm = None
        self.logger = logging.getLogger('doppler_experiment')

        self.result_manager = ResultManager(self.create_result_creators(), self.experiment_callback)

    def start(self):
        super(BaseMode, self).start()
        self.result_manager.clear()
        self.started = True
        self.fsm.max_points = self.settings['point_count']
        self.fsm.settings = {'start_v': self.settings['start_velocity'], 'end_v': self.settings['end_velocity']}
        self.fsm.start = True

    def stop(self):
        super(BaseMode, self).stop()
        self.result_manager.clear()
        self.started = False
        self.fsm.state = self.FSM.States.IDLE
        self.fsm.result_valid = False

    def tear_down(self):
        self.power_down()

    def power_down(self):
        self.doppler_driver = None
        self.result_manager.clear()

    def power_up(self):
        self.doppler_driver = self.DriverToUse(self.config[self.device_id]['port'],
                                               int(self.config[self.device_id]['timeout']))
        self.doppler_driver.logger = self.logger
        self.fsm = self.FSM(self.doppler_driver, self.config[self.device_id])

    @property
    def DriverToUse(self):
        if self.config.getboolean(self.device_id, "use_mock", fallback=bool(os.environ.get('DOPPLER_USE_MOCK', False))):
            return DopplerEffectMockDriver
        else:
            return DopplerEffectDriver

    @classmethod
    @abstractmethod
    def create_result_creators(cls):
        pass


class NormalMode(BaseMode):
    name = 'doppler'
    label = 'Doppler'

    time_counter = 0

    @classmethod
    def get_description(cls) -> str:
        pass

    @classmethod
    def get_mode_label(cls):
        return cls.label

    @classmethod
    def get_mode_name(cls) -> str:
        return cls.name

    def loop_iteration(self):
        if self.started:
            self.fsm.process()

            if self.fsm.result_valid:
                self.result_manager.push_results(self.fsm.result)

            if self.fsm.finished:
                self.experiment_callback.send_series_done()

    @classmethod
    def get_series_name(cls, settings):
        return "{} cm/s - {} cm/s".format(settings['start_velocity'], settings['end_velocity'])

    @classmethod
    def get_output_fields(cls):
        return OutputFieldSuite(
            chart=ChartField("chart", ["chart"], _("CharLabel"), _("Velocity"), _("Frequency")),
            # zero_freq=OutputField("integer-indicator", ['zero_freq'], label="Częstotliwość generatora [Hz]")
            # current_vel = OutputField("integer-indicator", ['velocity'], label="Prędkość [RPM]"),
            # curent_freq = OutputField("integer-indicator", ["freq"], label="Częstotliwość [Hz]"),
            )

    @classmethod
    def get_input_fields(cls) -> ControlSuite:
        return ControlSuite(
            NumberControl("start_velocity", _("StartVelocity"),
                          description=_("StartVelocityDesc"),
                          min_value=50,
                          max_value=70,
                          default_value=50),
            #              description="Prędkość na której kończysz eksperyment [cm/s]",
            NumberControl("end_velocity", _("EndVelocity"),
                          description=_("EndVelocityDesc"),
                          min_value=71,
                          max_value=110,
                          default_value=110),
            NumberControl("point_count", _("PointCount"),
                          description=_("PointCountDesc"),
                          min_value=1,
                          max_value=100,
                          default_value=20),
        )

    @classmethod
    def create_result_creators(cls):
        return [
            XYChartResultCreator("chart", read_from=["v", "f"]),
            # OverrideResultsCreator("zero_freq", read_from=["zero_f"]),
        ]

    #RESULT_CREATORS = [
    #    ReturnLastElementResultCreator("velocity"),
    #    ReturnLastElementResultCreator("freq"),
    #    XYChartResultCreator("chart", read_from=["velocity", 'freq']),
    #]


class DopplerManager(MultiModeManager):
    EXPERIMENT_NAME = "Doppler"

    MODE_MANAGERS = [NormalMode, ]

    def post_power_up_diagnostics(self, diagnostics_level='short'):
        pass

    @classmethod
    def get_mode_managers(cls, config_parser: configparser.ConfigParser) -> typing.List['ExperimentMode']:
        return cls.MODE_MANAGERS

    def pre_power_up_diagnostics(self, diagnostics_level='short'):
        pass
