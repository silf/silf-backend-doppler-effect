# coding=utf-8
import abc


class DopplerEffectAPI(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def start(self):
        """
        """
    @abc.abstractmethod
    def get_device_model(self):
        """
        """

    @abc.abstractmethod
    def set_velocity(self, velocity):
        """
        Sets velocity.
        range of value must be checked before calling this method

        :param velocity: in volts, 0-4095
        :return:
        """

    @abc.abstractmethod
    def get_velocity(self):
        """
        Read velocity

        :return: velocity
        """

    @abc.abstractmethod
    def get_time(self):
        """
        Read time between sensor triggers

        :return: time in ms.
        """

    @abc.abstractmethod
    def trigger(self):
        """
        Trigger the experiment
        """

    @property
    @abc.abstractmethod
    def is_triggering(self):
        """
        Not used method/property. Reserved for future
        Check if experiment is triggerring - after start (trigger) of the experiment
        some time is needed to set desired velocity (the experiment is in state RUNING).
        When velocity is set the device returns "OK\n" - the experiment is in state READY

        :return: True - the experiment is in triggering proccess
                 False - the experiment is triggered
        """

    @abc.abstractmethod
    def get_freq(self):
        """
        Read freq

        :return: freq
        """

    @abc.abstractmethod
    def measure_frequency_zero(self):
        """
        Measure frequency with stopped cart

        :return: frequency
        """

    @abc.abstractmethod
    def stop(self):
        """
        """

    @abc.abstractmethod
    def is_open(self):
        """
        """

    @abc.abstractmethod
    def close(self):
        """
        """
