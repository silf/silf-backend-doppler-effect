# coding=utf-8
from time import sleep
import random

from silf.backend.doppler.api import DopplerEffectAPI


class DopplerEffectMockDriver(DopplerEffectAPI):

    def __init__(self, deviceFilePath, readTimeout=10):
        super(DopplerEffectMockDriver, self).__init__()
        self.velocity = 0
        self.freq = 0
        self.__triggering = False
        self.logger = None

    def start(self):
        pass

    def get_device_model(self):
        return 'DopplerEffect\n'

    def set_velocity(self, velocity):
        self.logger.debug("Set vel: %s", velocity)
        self.velocity = velocity

    def trigger(self):
        # self.__triggering = True Ta linijka i tak jest noopem
        sleep(1)
        self.__triggering = False

    @property
    def is_triggering(self):
        return self.__triggering

    def get_velocity(self):
        self.logger.debug("Get vel: %s", self.velocity)
        return self.velocity

    def get_time(self):
        return 2/self.velocity * 1000

    def get_freq(self):
        #self.freq=random.randint(1, 5)
        self.freq=self.velocity
        self.logger.debug("Get freq: %s", self.freq)
        return self.freq

    def measure_frequency_zero(self):
        return 40052.80

    def stop(self):
        pass

    def is_open(self):
        return True

    def close(self):
        pass
